# Tic Tac Toe 
Tic-Tac-Toe Game in python3 Tkinter

## About:
***Tic Tac Toe or Noughts and Crosses*** as called in British is a pencil and paper game for two player. The player who succed in placing three of their marks in a single line either horizontally, vertically or diagonally wins the game. In this Repository I make the game using Tkinter in Python. Here two human are playing against each other.Tkinter is a GUI framework for Python that allows for buttons, graphics and alot of other things to be done in a window
<br>

<p align="center">Tic-Tac-Toe<br>
  <img src="images/1st.png" width="350" height="450"><p/>

## Demo:

<p align="center" width="100%">
    <img width="35%" height="400" src="images/2nd.png">
    <img width="35%" height="400" src="images/3rd.png">
</p>
