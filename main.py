from tkinter import *
from tkinter import messagebox
import random as r

def button(frame):
    b=Button(frame,padx=1,pady=25,bg="#48cae4",width=3,text=" ",font=('arial',55,'bold'),relief="sunken",bd=5)
    return b

def change_a():
    global a,count
    count +=1
    if a == 'O':
        a='X'
    else:
        a='O'

def reset():
    global a,count
    for i in range(3):
        for j in range(3):
                b[i][j]["text"]=" "
                b[i][j]["state"]=NORMAL
    a=r.choice(['O','X'])
    count = 0


def check():                #Checks for victory or Draw
    for i in range(3):
            if(b[i][0]["text"]==b[i][1]["text"]==b[i][2]["text"]==a or b[0][i]["text"]==b[1][i]["text"]==b[2][i]["text"]==a):
                    messagebox.showinfo("Congrats!!","'"+a+"' has won")
                    reset()
    if(b[0][0]["text"]==b[1][1]["text"]==b[2][2]["text"]==a or b[0][2]["text"]==b[1][1]["text"]==b[2][0]["text"]==a):
        messagebox.showinfo("Congrats!!","'"+a+"' has won")
        reset()
    elif count == 9 :
        messagebox.showinfo("Tied!!","The match ended in a draw")
        reset()

def click(row,col):
        b[row][col].config(text=a,state=DISABLED,disabledforeground=colour[a])
        check()
        change_a()
        label.config(text=a+"'s Chance")

root=Tk()
root.title("Tic-Tac-Toe")
root.configure(bg='#00b4d8')
root.resizable(0,0)
count = 0
a=r.choice(['O','X'])
colour={'O':"red",'X':"yellow"}
b=[[],[],[]]

for i in range(3):
        for j in range(3):
                b[i].append(button(root))
                b[i][j].config(command= lambda row=i,col=j:click(row,col))
                b[i][j].grid(row=i,column=j)
label=Label(text=f"{a}'s Chance",bg='#00b4d8',font=('arial',20,'bold'))
label.grid(row=3,column=0,columnspan=3)
root.mainloop()